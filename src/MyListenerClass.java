import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * Created by mohitnag on 31/1/18.
 */
public class MyListenerClass implements ITestListener{

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("Test - " + iTestResult.getName()+" started");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("Test - " + iTestResult.getName()+" succeded");

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("Test - " + iTestResult.getName() +" failed");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
