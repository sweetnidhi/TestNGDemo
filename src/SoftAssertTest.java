import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by mohitnag on 1/2/18.
 */
@Listeners(MyListenerClass.class)
public class SoftAssertTest {
    @Test
    public  void test()
    {
        System.out.println("test");
        Assert.assertTrue(false);
        System.out.println("Line after failing assert");
    }
    @Test
    public  void est()
    {
        System.out.println("est");
        SoftAssert sa = new SoftAssert();
        sa.assertTrue(false);
        System.out.println("Line after failing soft asser");
        sa.assertEquals(12,123);
        System.out.println("Line after another failing assert");
        System.out.println("Note that test passed even if 2 asserts failed");
    }
    @Test
    public  void st()
    {
        System.out.println("st");
        SoftAssert sa = new SoftAssert();
        sa.assertTrue(false);
        System.out.println("Line after failing soft asser");
        sa.assertEquals(12,123);
        System.out.println("Line after another failing assert");
        System.out.println("Note that test failed as we used assertAll");
        sa.assertAll();
        System.out.println("Line after assertAll");
    }

}
