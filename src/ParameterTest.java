import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Created by mohitnag on 1/2/18.
 */
@Listeners(MyListenerClass.class)
public class ParameterTest {
    @Parameters("shape")
    @Test
    public  void test(String shapeArg)
    {
        System.out.println("test");
        switch (shapeArg)
        {
            case "Square":
                System.out.println("Square");
                break;
            case "Circle":
                System.out.println("Circle");
                break;
        }

    }
}
