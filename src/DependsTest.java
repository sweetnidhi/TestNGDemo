import org.testng.annotations.Test;

public class DependsTest
{
    @Test
        public void test() {
        System.out.println("test");
    }

        @Test(dependsOnMethods = "test",priority = 2)
        public void est() {
        System.out.println("est");
    }

        @Test(dependsOnMethods = "test",priority = 1)
        public void st() {
        System.out.println("st");
    }
}